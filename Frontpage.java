import java.awt.event.*;
import java.util.Enumeration;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class Frontpage extends JPanel {

    private JLabel nimi, ohje, infoteksti, vaikeustaso, kuva, teemakuva, kielikuva;
    private JPanel panel, hirsipuukuva, teemakuvapanel, kielikuvapanel;
    private JButton info, aloita;
    private static JComboBox cbteema, cbkieli;
    private JRadioButton helppo, keskitaso, vaikea;
    private static ButtonGroup vaihtoehto;
    private JTextArea infoteksti2;
    private Icon kysymysmerkki;

    public Frontpage() {

        // Luodaan hirsipuu teksti
        nimi = new JLabel("HIRSIPUU", SwingConstants.CENTER);
        nimi.setBounds(300, 30, 200, 30);
        nimi.setFont(new Font("SansSerif", Font.BOLD, 25));
        this.add(nimi);
        // Luodaan infotesksti ja infopaneeli
        ohje = new JLabel("<html>OHJE</html>", SwingConstants.CENTER);
        ohje.setFont(new Font("SansSerif", Font.BOLD, 16));

        infoteksti = new JLabel("<html> <b>X</b>:Sulkee pelin.<br>" +
                "<br>TEEMA :Valitse teema jonka<br> mukaan sanasi määräytyy.<br>" +
                "<br>KIELI :Valitse millä kielellä <br>haluat sanasi tulevan.<br>" +
                "<br><b>VAIKEUSTASO</b>: valitse yksi<br>" +
                "kolmesta vaikeustasosta,<br>" +
                "arvattavien sanojen vaikeustaso<br>" +
                "määräytyy valintasi mukaan.<br>" +
                "<br><b>ALOITA</b>: Aloittaa pelin.</html>", SwingConstants.LEFT);
        infoteksti.setFont(new Font("SansSerif", Font.PLAIN, 14));

        panel = new JPanel();
        panel.setLayout(null);
        panel.setBounds(110, 30, 230, 320);
        panel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        panel.setBackground(Color.LIGHT_GRAY);
        panel.add(ohje);
        panel.add(infoteksti);
        infoteksti.setBounds(10, 20, 200, 300);
        ohje.setBounds(85, 5, 50, 25);
        this.add(panel);
        panel.setVisible(false);
        // Lisätään hirsipuukuva
        hirsipuukuva = new JPanel();
        LayoutManager overlay = new OverlayLayout(hirsipuukuva);
        hirsipuukuva.setLayout(overlay);
        hirsipuukuva.setBounds(225, 60, 350, 300);
        kuva = new JLabel(new ImageIcon(Peli.class.getResource("10.png")));
        hirsipuukuva.add(kuva);
        add(hirsipuukuva);
        // lisätään kysymysmerkki ikoni ja luodaan painike
        kysymysmerkki = new ImageIcon(Peli.class.getResource("question.png"));
        info = new JButton(kysymysmerkki);
        info.setBounds(50, 30, 50, 50);
        info.setBorder(new RoundBorder(80));
        info.setOpaque(false);
        info.setContentAreaFilled(false);
        info.addMouseListener(new MouseAdapter() {
            public void mouseEntered(MouseEvent e) {
                panel.setVisible(true);
            }

            public void mouseExited(MouseEvent evt) {
                panel.setVisible(false);
            }
        });
        add(info, BorderLayout.NORTH);
        // luodaan teemalle ikoni
        teemakuvapanel = new JPanel();
        LayoutManager overlay2 = new OverlayLayout(teemakuvapanel);
        teemakuvapanel.setLayout(overlay2);
        teemakuvapanel.setBounds(490, 360, 25, 25);
        teemakuva = new JLabel(new ImageIcon(Peli.class.getResource("aivot.png"))); // Image provided by Marc Steiner
        teemakuvapanel.add(teemakuva);
        add(teemakuvapanel);
        // luodaan ikoni kielelle
        kielikuvapanel = new JPanel();
        LayoutManager overlay3 = new OverlayLayout(kielikuvapanel);
        kielikuvapanel.setLayout(overlay3);
        kielikuvapanel.setBounds(290, 360, 25, 25);
        kielikuva = new JLabel(new ImageIcon(Peli.class.getResource("lippu.png")));
        kielikuvapanel.add(kielikuva);
        add(kielikuvapanel);
        // Luodaan combobox jossa valitaan pelille teema
        String teemat[] = { "AMMATIT", "ELÄIMET" };
        cbteema = new JComboBox<>(teemat);
        cbteema.setBounds(450, 390, 100, 30);
        this.add(cbteema);
        // Luodaan combobox jossa valitaan pelille kieli.
        String kieli[] = { "SUOMI", "ENGLANTI" };
        cbkieli = new JComboBox<>(kieli);
        cbkieli.setBounds(250, 390, 100, 30);
        this.add(cbkieli);
        // lisätään vaikestaso teksti
        vaikeustaso = new JLabel("VAIKEUSTASO");
        vaikeustaso.setBounds(345, 430, 120, 30);
        vaikeustaso.setFont(new Font("SansSerif", Font.BOLD, 14));
        this.add(vaikeustaso);
        // luodaan radiobuttonit jossa valitaan pelin vaikeustaso
        helppo = new JRadioButton("HELPPO");
        helppo.setActionCommand("HELPPO");
        helppo.setBounds(250, 455, 100, 30);
        this.add(helppo);
        helppo.setSelected(true);

        keskitaso = new JRadioButton("KESKITASO");
        keskitaso.setActionCommand("KESKITASO");
        keskitaso.setBounds(350, 455, 100, 30);
        this.add(keskitaso);

        vaikea = new JRadioButton("VAIKEA");
        vaikea.setActionCommand("VAIKEA");
        vaikea.setBounds(475, 455, 100, 30);
        this.add(vaikea);

        vaihtoehto = new ButtonGroup();
        vaihtoehto.add(helppo);
        vaihtoehto.add(keskitaso);
        vaihtoehto.add(vaikea);
        // Luodaan ja lisätään pelin aloittamis näppäin.
        aloita = new JButton("ALOITA");
        aloita.setBounds(350, 500, 100, 30);
        aloita.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Peli.vaihdaVaikeustasojaTeema();
                Main.command();
            }
        });
        this.add(aloita);

        this.setSize(800, 600);
        this.setLayout(null);
        this.setVisible(true);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(800, 600);
    }

    public static String valittuVaikeustaso() {
        // palauttaa valitun vaikeustason
        String apua = vaihtoehto.getSelection().getActionCommand();
        
        return apua;
    }

    public static String valittuTeema() {
        // palauttaa valitun teeman
        return cbteema.getSelectedItem().toString();
    }

    public static String valittuKieli() {
        // palauttaa valitun kielen
        return cbkieli.getSelectedItem().toString();
    }

}
