import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Random;

public class GameLogic {

    static ArrayList<String> words;
    static String sana;
    static ArrayList<Character> sanaListana;

    public static String getWord(String vaikeus) {
        // metodi joka lukee tiedostosta sanan ja valitsee randomilla niistä yhden ja
        // palauttaa sen.

        words = new ArrayList<String>();
        sanaListana = new ArrayList<Character>();

        // luetaan sanat tiedostosta
        try (FileInputStream fis = new FileInputStream("sanat" + "/" + vaikeus);
                InputStreamReader isr = new InputStreamReader(fis, StandardCharsets.UTF_8);
                BufferedReader reader = new BufferedReader(isr)) {

            String str;
            while ((str = reader.readLine()) != null) {
                words.add(str);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        Random r = new Random();
        int randomi = r.nextInt(words.size());
        sana = (String) words.get(randomi);
        char ch;

        // looppi laittaa sanan kirjain kerrallaan sanaListana listaan
        for (int f = 0; f < sana.length(); f++) {
            ch = sana.charAt(f);
            if (Character.isLowerCase(ch)) {
                ch = Character.toUpperCase(ch);
            }
            sanaListana.add(ch);
        }

        System.out.println("Tiedostosta " + sana);
        return sana;
    }

    public static boolean[] checkForMatch(char letter) {
        // metodi joka palauttaa boolean taulukon sen perusteella onko arvattu kirjain
        // arvattavassa sanassa.

        boolean[] palauta = new boolean[sanaListana.size()];
        // looppi joka tarkastaa onko kirjain arvattavassa sanassa
        // palauta taulukkoon laitetaan true arvo niille paikoille, missä arvattavassa
        // sanassa sijaitsee arvattu kirjain
        for (int i = 0; i < sanaListana.size(); i++) {

            char kirjain = sanaListana.get(i);

            if (kirjain == letter) {
                palauta[i] = true;
            } else {
                palauta[i] = false;
            }
        }
        return palauta;
    }

    public static boolean onkoVoitettu(char[] tyhjasana) {
        // metodi joka tarkastaa onko peli voitettu
        for (char i : tyhjasana) {
            if (i == '-') {
                return true;
            }
        }
        return false;
    }
}
