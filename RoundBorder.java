import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;

import java.awt.*;

class RoundBorder implements Border{
    // Tekee info ja takaisin napista pyöreät

    private int i;

    RoundBorder(int i){
        this.i = i;
    }

    public void paintBorder(Component c, Graphics g, int x, int y, 
                            int width, int height) {
    g.drawRoundRect(x, y, width-1, height-1, i, i);
    }

    @Override
    public Insets getBorderInsets(Component c) {
        return new Insets(this.i+1, this.i+1, this.i+2, this.i);
    }

    @Override
    public boolean isBorderOpaque() {
        return false;
    }
}
