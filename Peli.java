import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.border.*;
import java.io.*;
import java.util.ArrayList;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.awt.event.*;

public class Peli extends JPanel {
    private JPanel varmistus, kyllaPeruutaButtons, infoPanel, hirsipuuKuva, kirjaimet;
    private static JPanel voitto, arvattuJo, havio;

    private JButton kylla, peruuta, takaisinNappi, infoNappi;

    private static JLabel text, varmistusteksti, ohje, edistyminen, viivat, label2;
    private static JLabel kuva1, kuva2, kuva3, kuva4, kuva5, kuva6, kuva7, kuva8, kuva9, kuva10;

    private static String theme = Frontpage.valittuTeema();
    private static String vaikeus = Frontpage.valittuVaikeustaso();
    private static String kieli = Frontpage.valittuKieli();
    private static String oikeasana, salainenSana, kirjain;

    private static char[] tyhjaSana;
    private static ArrayList<Character> arvatut;

    private static int arvaukset = 9;

    private static JLabel aa, bb, cc, dd, ee, ff, gg, hh, ii, jj, kk, ll, mm, nn, oo, pp,
            qq, rr, ss, tt, uu, vv, ww, xx, yy, zz, ruoto, pistea, pisteo;

    public Peli() {
        // pyörittää itse peliä

        // luodaan arvatut lista, jota käytetään apuna kun tarkastetaan onko arvattu
        // kirjain arvattu jo aikaisemmin
        arvatut = new ArrayList<Character>();

        // luodaan paneeli joka näytetään kun pelaaja voittaa pelin
        voitto = new JPanel();
        voitto.setBounds(200, 200, 400, 150);
        voitto.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        JLabel voitit = new JLabel("VOITIT!");
        voitit.setBounds(165, 20, 100, 50);
        voitit.setFont(new Font("SansSerif", Font.BOLD, 20));
        JLabel sanaOli = new JLabel("Sana oli " + oikeasana);
        sanaOli.setBounds(155, 50, 200, 50);
        JPanel menuSanaButtons = new JPanel();

        JButton menuB = new JButton("MENU");
        JButton uusiSanaB = new JButton("UUSI SANA");
        menuSanaButtons.add(menuB);
        menuSanaButtons.add(uusiSanaB);
        menuB.setBounds(25, 0, 125, 30);
        uusiSanaB.setBounds(200, 0, 125, 30);
        menuSanaButtons.setBounds(25, 100, 350, 41);
        menuSanaButtons.setLayout(null);

        menuB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                voitto.setVisible(false);

                Main.frontpageVisible();
            }
        });

        uusiSanaB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                voitto.setVisible(false);
                Peli.vaihdaVaikeustasojaTeema();
                Main.command();

            }
        });
        voitto.add(voitit);
        voitto.add(sanaOli);
        voitto.add(menuSanaButtons);
        voitto.setVisible(false);
        voitto.setLayout(null);
        add(voitto);
        // luodaan paneeli joka näytetään kun käyttäjä häviää pelin
        havio = new JPanel();
        havio.setBounds(200, 200, 400, 150);
        havio.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        JLabel havisit = new JLabel("HÄVISIT!");
        havisit.setBounds(165, 20, 100, 50);
        JLabel tokasanaOli = new JLabel("Sana oli " + oikeasana);
        havisit.setFont(new Font("SansSerif", Font.BOLD, 20));
        tokasanaOli.setBounds(155, 50, 200, 50);
        JPanel tokamenuSanaButtons = new JPanel();

        JButton tokamenuB = new JButton("MENU");
        JButton tokauusiSanaB = new JButton("UUSI SANA");
        tokamenuSanaButtons.add(tokamenuB);
        tokamenuSanaButtons.add(tokauusiSanaB);
        tokamenuB.setBounds(25, 0, 125, 30);
        tokauusiSanaB.setBounds(200, 0, 125, 30);
        tokamenuSanaButtons.setBounds(25, 100, 350, 41);
        tokamenuSanaButtons.setLayout(null);

        tokamenuSanaButtons.add(tokamenuB);
        tokamenuSanaButtons.add(tokauusiSanaB);

        tokamenuB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                havio.setVisible(false);

                Main.frontpageVisible();
            }
        });

        tokauusiSanaB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                havio.setVisible(false);
                Peli.vaihdaVaikeustasojaTeema();
                Main.command();

            }
        });

        havio.add(havisit);
        havio.add(tokasanaOli);
        havio.add(tokamenuSanaButtons, BorderLayout.SOUTH);
        havio.setVisible(false);
        havio.setLayout(null);
        add(havio);
        // luodaan paneeli joka näytetään kun käyttäjä arvaa kirjainta minkä on jo
        // aikaisemmin arvannut
        arvattuJo = new JPanel();
        arvattuJo.setBounds(200, 200, 400, 150);
        arvattuJo.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        JButton palaaPeliinButton = new JButton("PALAA PELIIN");
        palaaPeliinButton.setBounds(100, 100, 200, 40);
        JLabel oletarvannut = new JLabel("Olet arvannut tämän kirjaimen jo kerran");
        oletarvannut.setBounds(100, 30, 300, 50);
        arvattuJo.add(oletarvannut);
        arvattuJo.add(palaaPeliinButton);
        arvattuJo.setVisible(false);
        arvattuJo.setLayout(null);
        add(arvattuJo);

        palaaPeliinButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                arvattuJo.setVisible(false);
            }
        });
        // luodaan paneeli joka näytetään kun käyttäjä painaa takaisin- näppäintä
        varmistus = new JPanel();
        varmistus.setBounds(200, 200, 400, 150);
        varmistus.setBorder(BorderFactory.createLineBorder(Color.GRAY));

        kylla = new JButton("KYLLÄ");
        kylla.setBounds(250, 350, 50, 20);
        peruuta = new JButton("PERUUTA");
        peruuta.setBounds(350, 350, 50, 20);

        kyllaPeruutaButtons = new JPanel();
        kyllaPeruutaButtons.add(kylla);
        kyllaPeruutaButtons.add(peruuta);

        varmistusteksti = new JLabel("Haluatko varmasti palata menuun?");
        varmistusteksti.setFont(new Font("SansSerif", Font.BOLD, 20));
        edistyminen = new JLabel("Edistymisesi tässä sanassa menetetään");
        peruuta.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                varmistus.setVisible(false);
            }
        });

        kylla.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                varmistus.setVisible(false);

                setVisible(false);
            }
        });

        varmistus.add(varmistusteksti);
        varmistus.add(edistyminen);
        varmistus.add(kyllaPeruutaButtons, BorderLayout.SOUTH);
        add(varmistus);
        varmistus.setVisible(false);
        // luodaan takaisin nappi
        Icon arrow = new ImageIcon(Peli.class.getResource("nuoli.png"));
        takaisinNappi = new JButton(arrow);
        takaisinNappi.setBounds(50, 30, 50, 50);
        takaisinNappi.setBorder(new RoundBorder(80));
        takaisinNappi.setOpaque(false);
        takaisinNappi.setContentAreaFilled(false);
        takaisinNappi.setMargin(new Insets(0, 0, 0, 0));
        takaisinNappi.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                varmistus.setVisible(true);
            }
        });
        // ohje teksti joka näytetään kun pelaaja siirtää kursorin ohje napin päälle
        ohje = new JLabel("<html>OHJE</html>", SwingConstants.CENTER);
        ohje.setFont(new Font("SansSerif", Font.BOLD, 16));

        JLabel infoteksti = new JLabel("<html>Yläreunassa näet aiemmin valitemasi teeman ja" +
                "<br>vaikeustason.<br>" +
                "<br> <b>&lt</b>: Palaa menuun.<br>" +
                "<br> <b>X</b>: Sulkee pelin.<br>" +
                "<br> <b>Arvatut kirjaimet</b> näyttää aiemmin jo arvaamasi<br>" +
                "kirjaimet.<br>" +
                "<br> <b>Arvauksia jäljellä</b> näyttää montako arvausta sinulla<br>" +
                "on jäljellä tässä sanassa.<br>" +
                "<br>_ _ _ merkit alareunassa näyttävät montako kirjainta" +
                " sanassa on ja aikaisemmin oikein arvaamasi kirjaimet" +
                " ilmestyvät viivoille niille paikoille, missä ne kyseisessä" +
                " sanassa sijaitsevat. <br>" +
                "<br> <b>Syötä kirjain</b>: Vieressä olevaan laatikkoon syötät" +
                " kirjaimen minkä haluat arvata, ja painat ENTER" +
                " näppäintä hyväksyäsi sen.</html>", SwingConstants.LEFT);
        infoteksti.setFont(new Font("SansSerif", Font.PLAIN, 14));

        infoPanel = new JPanel();
        infoPanel.setLayout(null);
        infoPanel.setBounds(180, 30, 360, 450);
        infoPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        infoPanel.setBackground(Color.LIGHT_GRAY);
        infoPanel.add(ohje);
        infoPanel.add(infoteksti);
        infoteksti.setBounds(10, 30, 350, 400);
        ohje.setBounds(155, 5, 50, 25);
        this.add(infoPanel);
        infoPanel.setVisible(false);
        // luodaan ohje nappi
        Icon kysymysmerkki = new ImageIcon(Peli.class.getResource("question.png"));
        infoNappi = new JButton(kysymysmerkki);
        infoNappi.setBounds(120, 30, 50, 50);
        infoNappi.setBorder(new RoundBorder(80));
        infoNappi.setOpaque(false);
        infoNappi.setContentAreaFilled(false);
        infoNappi.setMargin(new Insets(0, 0, 0, 0));
        infoNappi.addMouseListener(new MouseAdapter() {
            public void mouseEntered(MouseEvent e) {
                infoPanel.setVisible(true);
            }

            public void mouseExited(MouseEvent evt) {
                infoPanel.setVisible(false);
            }
        });
        // lisätään päivittyvä hirsipuukuva, joka etenee sitä mukaa kun pelaaja arvaa
        // kirjaimia väärin
        hirsipuuKuva = new JPanel();
        LayoutManager overlay = new OverlayLayout(hirsipuuKuva);
        hirsipuuKuva.setLayout(overlay);
        hirsipuuKuva.setBounds(50, 100, 350, 300);
        text = new JLabel("Arvauksia jäljellä " + arvaukset);
        text.setBounds(540, 350, 150, 50);
        text.setFont(new Font("SansSerif", Font.BOLD, 16));
        add(text);

        kuva1 = new JLabel(new ImageIcon(Peli.class.getResource("1.png")));
        kuva2 = new JLabel(new ImageIcon(Peli.class.getResource("2.png")));
        kuva3 = new JLabel(new ImageIcon(Peli.class.getResource("3.png")));
        kuva4 = new JLabel(new ImageIcon(Peli.class.getResource("4.png")));
        kuva5 = new JLabel(new ImageIcon(Peli.class.getResource("5.png")));
        kuva6 = new JLabel(new ImageIcon(Peli.class.getResource("6.png")));
        kuva7 = new JLabel(new ImageIcon(Peli.class.getResource("7.png")));
        kuva8 = new JLabel(new ImageIcon(Peli.class.getResource("8.png")));
        kuva9 = new JLabel(new ImageIcon(Peli.class.getResource("9.png")));
        kuva10 = new JLabel(new ImageIcon(Peli.class.getResource("10.png")));

        // lisätään kuvat paneeliin, ja asetetaan niitten näkyvyys
        hirsipuuKuva.add(kuva1);
        kuva1.setVisible(true);
        hirsipuuKuva.add(kuva2);
        kuva2.setVisible(false);
        hirsipuuKuva.add(kuva3);
        kuva3.setVisible(false);
        hirsipuuKuva.add(kuva4);
        kuva4.setVisible(false);
        hirsipuuKuva.add(kuva5);
        kuva5.setVisible(false);
        hirsipuuKuva.add(kuva6);
        kuva6.setVisible(false);
        hirsipuuKuva.add(kuva7);
        kuva7.setVisible(false);
        hirsipuuKuva.add(kuva8);
        kuva8.setVisible(false);
        hirsipuuKuva.add(kuva9);
        kuva9.setVisible(false);
        hirsipuuKuva.add(kuva10);
        kuva10.setVisible(false);
        add(hirsipuuKuva);
        // luodaan kirjaimet, ja niille paneeli
        // nämä näytetään kun käyttäjä arvaa kyseisen kirjaimen.
        kirjaimet = new JPanel();

        kirjaimet.setBounds(525, 140, 170, 150);
        aa = new JLabel("A");
        bb = new JLabel("B");
        cc = new JLabel("C");
        dd = new JLabel("D");
        ee = new JLabel("E");
        ff = new JLabel("F");
        gg = new JLabel("G");
        hh = new JLabel("H");
        ii = new JLabel("I");
        jj = new JLabel("J");
        kk = new JLabel("K");
        ll = new JLabel("L");
        mm = new JLabel("M");
        nn = new JLabel("N");
        oo = new JLabel("O");
        pp = new JLabel("P");
        qq = new JLabel("Q");
        rr = new JLabel("R");
        ss = new JLabel("S");
        tt = new JLabel("T");
        uu = new JLabel("U");
        vv = new JLabel("V");
        ww = new JLabel("W");
        xx = new JLabel("X");
        yy = new JLabel("Y");
        zz = new JLabel("Z");
        ruoto = new JLabel("Å");
        pistea = new JLabel("Ä");
        pisteo = new JLabel("Ö");
        // asetetaan kirjainten fontti
        aa.setFont(new Font("SansSerif", Font.BOLD, 25));
        bb.setFont(new Font("SansSerif", Font.BOLD, 25));
        cc.setFont(new Font("SansSerif", Font.BOLD, 25));
        dd.setFont(new Font("SansSerif", Font.BOLD, 25));
        ee.setFont(new Font("SansSerif", Font.BOLD, 25));
        ff.setFont(new Font("SansSerif", Font.BOLD, 25));
        gg.setFont(new Font("SansSerif", Font.BOLD, 25));
        hh.setFont(new Font("SansSerif", Font.BOLD, 25));
        ii.setFont(new Font("SansSerif", Font.BOLD, 25));
        jj.setFont(new Font("SansSerif", Font.BOLD, 25));
        kk.setFont(new Font("SansSerif", Font.BOLD, 25));
        ll.setFont(new Font("SansSerif", Font.BOLD, 25));
        mm.setFont(new Font("SansSerif", Font.BOLD, 25));
        nn.setFont(new Font("SansSerif", Font.BOLD, 25));
        oo.setFont(new Font("SansSerif", Font.BOLD, 25));
        pp.setFont(new Font("SansSerif", Font.BOLD, 25));
        qq.setFont(new Font("SansSerif", Font.BOLD, 25));
        rr.setFont(new Font("SansSerif", Font.BOLD, 25));
        ss.setFont(new Font("SansSerif", Font.BOLD, 25));
        tt.setFont(new Font("SansSerif", Font.BOLD, 25));
        uu.setFont(new Font("SansSerif", Font.BOLD, 25));
        vv.setFont(new Font("SansSerif", Font.BOLD, 25));
        xx.setFont(new Font("SansSerif", Font.BOLD, 25));
        yy.setFont(new Font("SansSerif", Font.BOLD, 25));
        zz.setFont(new Font("SansSerif", Font.BOLD, 25));
        ruoto.setFont(new Font("SansSerif", Font.BOLD, 25));
        pistea.setFont(new Font("SansSerif", Font.BOLD, 25));
        pisteo.setFont(new Font("SansSerif", Font.BOLD, 25));
        ww.setFont(new Font("SansSerif", Font.BOLD, 25));
        // lisätään kirjaimet paneeliin

        kirjaimet.add(aa);
        kirjaimet.add(bb);
        kirjaimet.add(cc);
        kirjaimet.add(dd);
        kirjaimet.add(ee);
        kirjaimet.add(ff);
        kirjaimet.add(gg);
        kirjaimet.add(hh);
        kirjaimet.add(ii);
        kirjaimet.add(jj);
        kirjaimet.add(kk);
        kirjaimet.add(ll);
        kirjaimet.add(mm);
        kirjaimet.add(nn);
        kirjaimet.add(oo);
        kirjaimet.add(pp);
        kirjaimet.add(qq);
        kirjaimet.add(rr);
        kirjaimet.add(ss);
        kirjaimet.add(tt);
        kirjaimet.add(uu);
        kirjaimet.add(vv);
        kirjaimet.add(ww);
        kirjaimet.add(xx);
        kirjaimet.add(yy);
        kirjaimet.add(zz);
        kirjaimet.add(ruoto);
        kirjaimet.add(pistea);
        kirjaimet.add(pisteo);
        // asetetaan kirjainten näkyvyys

        aa.setVisible(false);
        bb.setVisible(false);
        cc.setVisible(false);
        dd.setVisible(false);
        ee.setVisible(false);
        ff.setVisible(false);
        gg.setVisible(false);
        hh.setVisible(false);
        ii.setVisible(false);
        jj.setVisible(false);
        kk.setVisible(false);
        ll.setVisible(false);
        mm.setVisible(false);
        nn.setVisible(false);
        oo.setVisible(false);
        pp.setVisible(false);
        qq.setVisible(false);
        rr.setVisible(false);
        ss.setVisible(false);
        tt.setVisible(false);
        uu.setVisible(false);
        vv.setVisible(false);
        xx.setVisible(false);
        yy.setVisible(false);
        zz.setVisible(false);
        ruoto.setVisible(false);
        pistea.setVisible(false);
        pisteo.setVisible(false);
        ww.setVisible(false);

        add(kirjaimet);
        // luodaan tekstit missä näkyy pelin teema, vaikeustaso sekä arvatut kirjaimet
        // teksti näyttämään
        // pelaajalle minne arvatut kirjaimet ilmestyy
        JLabel arvatut = new JLabel("Arvatut kirjaimet: ");
        arvatut.setFont(new Font("SansSerif", Font.BOLD, 16));
        arvatut.setBounds(550, 100, 150, 50);
        add(arvatut);

        JLabel teema = new JLabel(theme);
        teema.setFont(new Font("SansSerif", Font.BOLD, 20));
        teema.setBounds(450, 30, 150, 50);
        add(teema);

        JLabel vaikeustaso = new JLabel(vaikeus);
        vaikeustaso.setFont(new Font("SansSerif", Font.BOLD, 20));
        vaikeustaso.setBounds(299, 30, 150, 50);
        add(vaikeustaso);

        JLabel syotakirjain = new JLabel("Syota kirjain: ");
        syotakirjain.setFont(new Font("SansSerif", Font.BOLD, 14));
        syotakirjain.setBounds(300, 500, 100, 30);
        add(syotakirjain);
        // luodaan viivat joka näyttää montako kirjainta on sanassa, ja päivittyy sitä
        // mukaa kun kirjaimia arvataan
        viivat = new JLabel(salainenSana);
        viivat.setFont(new Font("SansSerif", Font.BOLD, 36));
        viivat.setBounds(350, 450, 200, 50);
        add(viivat);
        // luodaan tekstikenttä mihin pelaaja syöttää minkä kirjaimen haluaa arvata
        JTextField textField = new JTextField(1);
        // lisätään tekstikenttään rajoitus että siihen voi syöttää vain vain kirjaimia
        // ja vain yhen merkin
        textField.addKeyListener(new KeyAdapter() {
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();

                if (!(Character.isAlphabetic(c) || (c == KeyEvent.VK_BACK_SPACE) || c == KeyEvent.VK_DELETE)) {
                    e.consume();
                }

                kirjain = Character.toString(c);
                textField.setText("");
            }
        });
        // lisätään tekstikentän toiminnallisuus, mitä tapahtuu kun käyttäjä painaa
        // enter nappia näppäimistöllään
        Action action = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                kirjain = textField.getText();
                kirjainArvaus(kirjain);

            }
        };

        textField.setBounds(400, 500, 50, 30);
        textField.addActionListener(action);

        add(textField);
        add(infoNappi);
        add(takaisinNappi);

        setLayout(null);
    }

    public static void vaihdaVaikeustasojaTeema() {
        // metodi hakee frontpage oliosta mitkä ovat valittu vaikeustaso, teema ja
        // kieli.
        // ja hakee niiden perusteella gamelogic olion kautta sanan.
        vaikeus = Frontpage.valittuVaikeustaso();
        theme = Frontpage.valittuTeema();
        kieli = Frontpage.valittuKieli();
        arvaukset = 9;

        if (kieli.equals("SUOMI")) {
            if (vaikeus.equals("HELPPO")) {
                if (theme.equals("ELÄIMET")) {
                    oikeasana = GameLogic.getWord("elaimetHelppo.txt");

                } else {
                    oikeasana = GameLogic.getWord("ammatitHelppo.txt");

                }

            } else if (vaikeus.equals("KESKITASO")) {
                if (theme.equals("ELÄIMET")) {
                    oikeasana = GameLogic.getWord("elaimetKeski.txt");

                } else {
                    oikeasana = GameLogic.getWord("ammatitKeski.txt");

                }

            } else {
                if (theme.equals("ELÄIMET")) {
                    oikeasana = GameLogic.getWord("elaimetVaikea.txt");

                } else {
                    oikeasana = GameLogic.getWord("ammatitVaikea.txt");

                }

            }
        } else {
            if (vaikeus.equals("HELPPO")) {
                if (theme.equals("ELÄIMET")) {
                    oikeasana = GameLogic.getWord("elaimetEasy.txt");

                } else {
                    oikeasana = GameLogic.getWord("ammatitEasy.txt");

                }

            } else if (vaikeus.equals("KESKITASO")) {
                if (theme.equals("ELÄIMET")) {
                    oikeasana = GameLogic.getWord("elaimetMiddle.txt");

                } else {
                    oikeasana = GameLogic.getWord("ammatitMiddle.txt");

                }

            } else {
                if (theme.equals("ELÄIMET")) {
                    oikeasana = GameLogic.getWord("elaimetHard.txt");

                } else {
                    oikeasana = GameLogic.getWord("ammatitHard.txt");

                }

            }

        }
        // laitataan tyhjaSana listaan "-" jokaisen kirjaimen kohdalle sekä
        // laitetaan salainenSana stringiin nämä viivat, jotka pelaaja näkee.
        tyhjaSana = oikeasana.toCharArray();
        for (int x = 0; x < tyhjaSana.length; x++) {
            if (tyhjaSana[x] != ' ') {
                tyhjaSana[x] = '-';
            }
        }
        salainenSana = new String(tyhjaSana);

    }

    public static void kirjainArvaus(String kirjain) {
        // metodi joka tarkastaa onko arvattu kirjain sanassa ja tekee toimia sen mukaan

        // tehdään arvatusta kirjaimesta joka tulee String muodossa char, ja
        // varmistetaan että se on iso kirjain
        char uusikirjain = kirjain.charAt(0);
        if (Character.isLowerCase(uusikirjain)) {
            uusikirjain = Character.toUpperCase(uusikirjain);

        }
        // tarkastaa onko kirjain jo arvattu
        // jos kirjain on jo arvattu, metodin suoritus keskeytetään ja näytetään
        // käyttäjälle paneeli joka kertoo että kirjain on jo arvattu
        // jos kirjainta ei ole arvattu, jatketaan metodin suorittamista.
        for (int i = 0; i < arvatut.size(); i++) {
            if (uusikirjain == arvatut.get(i)) {
                arvattuJo.setVisible(true);
                return;
            }
        }
        //
        arvatut.add(uusikirjain);
        // luodaan boolean lista arvo, joka on arvatun sanan mittainen, ja siihen
        // laitetaan true niille paikoille missä
        // sijaitsee arvattu kirjain

        boolean[] testaaOikeus = GameLogic.checkForMatch(uusikirjain);
        boolean oikein = false;
        // looppi jossa tyhjaSana listaan lisätään arvattu kirjain oikeille paikoille
        for (int i = 0; i < testaaOikeus.length; i++) {
            if (testaaOikeus[i] == true) {
                tyhjaSana[i] = uusikirjain;
                oikein = true;
            }
        }
        // muutetaan salainenSana tyhjaSana listan mukaiseksi
        salainenSana = new String(tyhjaSana);
        // muutetaan viivat JLabel vastaamaan salaistasanaa
        // eli laitetaan arvattu kirjain oikeille paikoille
        viivat.setText(salainenSana);
        // Muutetaan arvattukirjain näkyväksi sille varatulle paikalle, missä käyttäjä
        // näkee kaikki arvaamansa kirjaimet
        kirjainNakyvaksi(uusikirjain);

        // tarkastaa onko peli voitettu tai hävitty, ja tarvittaessa muuttaa häviö tai
        // voitto paneelin näkyväksi käyttäjälle
        if (oikein) {
            if (GameLogic.onkoVoitettu(tyhjaSana) == false) {
                // tehdään voittopaneelin näkyväksi
                voitto.setVisible(true);

                System.out.println("");
            }
        } else {
            arvaukset--;
            vaihdaKuva();
            text.setText("Arvauksia jäljellä: " + arvaukset);

            if (arvaukset == 0) {
                // tehdään häviö paneeli näkyväksi
                havio.setVisible(true);
                System.out.println("Hävisit");
            }
        }
    }

    public static void kirjainNakyvaksi(char kirjain) {
        // muuttaa arvatun kirjaimen näkyvyyden
        switch (kirjain) {
            case 'A':
                aa.setVisible(true);
                break;
            case 'B':
                bb.setVisible(true);
                break;
            case 'C':
                cc.setVisible(true);
                break;
            case 'D':
                dd.setVisible(true);
                break;
            case 'E':
                ee.setVisible(true);
                break;
            case 'F':
                ff.setVisible(true);
                break;
            case 'G':
                gg.setVisible(true);
                break;
            case 'H':
                hh.setVisible(true);
                break;
            case 'I':
                ii.setVisible(true);
                break;
            case 'J':
                jj.setVisible(true);
                break;
            case 'K':
                kk.setVisible(true);
                break;
            case 'L':
                ll.setVisible(true);
                break;
            case 'M':
                mm.setVisible(true);
                break;
            case 'N':
                nn.setVisible(true);
                break;
            case 'O':
                oo.setVisible(true);
                break;
            case 'P':
                pp.setVisible(true);
                break;
            case 'Q':
                qq.setVisible(true);
                break;
            case 'R':
                rr.setVisible(true);
                break;
            case 'S':
                ss.setVisible(true);
                break;
            case 'T':
                tt.setVisible(true);
                break;
            case 'U':
                uu.setVisible(true);
                break;
            case 'V':
                vv.setVisible(true);
                break;
            case 'W':
                ww.setVisible(true);
                break;
            case 'X':
                xx.setVisible(true);
                break;
            case 'Y':
                yy.setVisible(true);
                break;
            case 'Z':
                zz.setVisible(true);
                break;
            case 'Å':
                ruoto.setVisible(true);
                break;
            case 'Ä':
                pistea.setVisible(true);
                break;
            case 'Ö':
                pisteo.setVisible(true);
                break;

        }
    }

    public static void vaihdaKuva() {
        // vaihtaa hirsipuukuvaa sitä mukaa kun käyttäjä arvaa kirjaimia väärin
        switch (arvaukset) {
            case 8:
                kuva2.setVisible(true);
                kuva1.setVisible(false);
                break;
            case 7:
                kuva3.setVisible(true);
                kuva2.setVisible(false);
                break;
            case 6:
                kuva4.setVisible(true);
                kuva3.setVisible(false);
                break;
            case 5:
                kuva5.setVisible(true);
                kuva4.setVisible(false);
                break;
            case 4:
                kuva6.setVisible(true);
                kuva5.setVisible(false);
                break;
            case 3:
                kuva7.setVisible(true);
                kuva6.setVisible(false);
                break;
            case 2:
                kuva8.setVisible(true);
                kuva7.setVisible(false);
                break;
            case 1:
                kuva9.setVisible(true);
                kuva8.setVisible(false);
                break;
            case 0:
                kuva10.setVisible(true);
                kuva9.setVisible(false);
                break;
        }
    }
}
