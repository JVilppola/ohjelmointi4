import java.awt.Dimension;

import java.awt.CardLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class Main extends JFrame {

    private JFrame window;
    private Frontpage frontpage;
    private static Peli peli;

    private static JPanel rootpanel;
    private static CardLayout cardlayout;

    public static void main(String[] args) {
        new Main().run();
    }

    private void run() {
        // luodaan JFrame ja rootpanel, jossa on cardlayout
        window = new JFrame("Hirsipuu");
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        cardlayout = new CardLayout();
        frontpage = new Frontpage();
        peli = new Peli();

        rootpanel = new JPanel(cardlayout);
        rootpanel.add(frontpage, "frontpage");
        rootpanel.add(peli, "peli");

        window.setPreferredSize(new Dimension(800, 600));

        window.add(rootpanel);

        window.pack();

        window.setVisible(true);
        window.setLocationRelativeTo(null);
        window.setResizable(false);

    }

    public static void command() {
        // tekee uuden peli-olion ja asettaa sen näkyväksei kättäjälle

        peli = new Peli();
        rootpanel.add(peli, "peli");

        cardlayout.show(rootpanel, "peli");
    }

    public static void frontpageVisible() {
        // muuttaa frontpagen käyttäjälle näkyväksi
        cardlayout.show(rootpanel, "frontpage");
    }

}
